module.exports = { 
    
    app: {
    	name: 'АгроМониторинг',
    	authRequired: true
    },

    session : {
        name: 'argo.sid',
        secret: 'agro',
    },

    server : {
        port : process.env.PORT || 3060
    },
    
    db : 'mongodb://localhost/agro', // using default mongodb port 27019
    
    cloudinary : {
        cloud_name: 'app-core',
        api_key: '544591776635331',
        api_secret: 'NwzZRgH8sQNaZyRC3jzE5I5xLH0',
        project_name: 'agro',
        photostorage: 'http://res.cloudinary.com/agro/image/upload'
    },
    
    hipchatter : {
    	key: 'B8B5L42UNibrjhq8udNDTqJvKC0irYBnzoGz5NSq',
		notify_key: 'B8B5L42UNibrjhq8udNDTqJvKC0irYBnzoGz5NSq'
    },

    socialAuth: {
		'facebookAuth' : {
			'clientID' 		: '749353061851568', // your App ID
			'clientSecret' 	: '5e4b8292b50f77193c264a956da5b0aa', // your App Secret
			'callbackURL' 	: 'http://pass.teamradar.org/auth/facebook/callback'
		},

		'twitterAuth' : {
			'consumerKey' 		: 'your-consumer-key-here',
			'consumerSecret' 	: 'rational-mote-91412',
			'callbackURL' 		: 'http://localhost:8080/auth/twitter/callback'
		},

		'googleAuth' : {
			'clientID' 		: '467478673770-5ssa54tvj91tk23h38p5tq20r02seucq.apps.googleusercontent.com',
			'clientSecret' 	: 'Waq7iGlnMuE1Bsx56p1Evk2L',
			'callbackURL' 	: 'http://teamradar.org/auth/google/callback'
		},

		'vkontakteAuth' : {
			'clientID' 		: '4876555',
			'clientSecret' 	: 'OqHDiJhOoomK2PllFby6',
			'callbackURL' 	: 'http://teamradar.org/auth/vkontakte/callback'
		}
	}
}
