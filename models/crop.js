var mongoose = require(__coreModules + '/mongoose');
var Schema = mongoose.Schema;

var schema = Schema({
	name: { 
		type: String,
        default: ''
    },
    color: { // css color
    	type: String,
        default: 'white'
    }
});

module.exports = mongoose.model('Crop', schema);
