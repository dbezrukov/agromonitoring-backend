var mongoose = require(__coreModules + '/mongoose');
var Schema = mongoose.Schema;

var schema = Schema({

    name: {
		type: String,
        default: ''
    },
    alias: {
		type: String,
        default: ''
    }, 

    admins: [{ type: Schema.ObjectId, ref: 'User' }],
    users: [{ type: Schema.ObjectId, ref: 'User' }],
    fields: [{ type: Schema.ObjectId, ref: 'Field' }]
});

module.exports = mongoose.model('Company', schema);
