var mongoose = require(__coreModules + '/mongoose');
var mongooseFill = require(__coreModules + '/mongoose-fill')

var Vegetation = require('./vegetation');

var Schema = mongoose.Schema;

var schema = Schema({
	name: {
		type: String,
        default: ''
    },
    group: { 
    	type: Schema.ObjectId, 
    	ref: 'Group' 
    },
	area: {
		official: Number,
		cultivated: Number,
		estimated: Number
	},
    place: {
		type: String,
		default: ''
	},
	polygon: {
		type: {	
			type: String, 
			required: true
		},
		coordinates: [Schema.Types.Mixed]
	}
	}, {
	toObject: { 
	 	virtuals: true,
	 	// toJSON produces undesirable id, so just delete it 
    	transform: function(doc, ret) {
			delete ret.id;
       	}
	},
    toJSON: { 
    	virtuals: true ,
    	// toJSON produces undesirable id, so just delete it 
    	transform: function(doc, ret) {
			delete ret.id;
       	}
    }
});

schema.fill('lastVegetation', function (callback) {
	
	Vegetation
        .find({ field: this.id })
        .select('date value')
        .sort('-date')
        .limit(1)
        .findOne({})
        .exec(callback)
})

module.exports = mongoose.model('Field', schema);