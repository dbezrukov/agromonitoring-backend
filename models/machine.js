var mongoose = require(__coreModules + '/mongoose');
var Schema = mongoose.Schema;

var schema = Schema({
	type: { 
		type: String,
        default: ''
    },
	name: { 
		type: String,
        default: ''
    },
    maker: { 
		type: String,
        default: ''
    },
    model: { 
		type: String,
        default: ''
    },
    inventory: { 
		type: String,
        default: ''
    },
    plate: { 
		type: String,
        default: ''
    },
    position: {
        lat: Number,
        lon: Number,
        valid: { 
            type : Boolean, 
            default: false
        },
        timestamp: { 
            type : Number, 
            default: 0
        }
    },
    track: [{ type: Schema.ObjectId, ref: 'Position' }],
});

module.exports = mongoose.model('Machine', schema);
