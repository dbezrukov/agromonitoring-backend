var mongoose = require(__coreModules + '/mongoose');
var Schema = mongoose.Schema;

var schema = Schema({
	field: { 
		type: Schema.ObjectId, ref: 'Field' 
	},
	year: {
		type: String,
		default: ''
	},
	cropId: {
		type: String,
        default: ''
    },
    yield: {
		type: String,
        default: ''
    },
	variety: {
		type: String,
        default: ''
    },
    tillage: {
		type: String,
        default: ''
    },
    dateSow: {
    	type: String, // 2016-04-20
        default: ''
    },
    dateHarvest: {
    	type: String,
        default: ''
    }
});

module.exports = mongoose.model('Seeding', schema);
