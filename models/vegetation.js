var mongoose = require(__coreModules + '/mongoose');
var Schema = mongoose.Schema;

var schema = Schema({
	field: { 
		type: Schema.ObjectId, ref: 'Field' 
	},
	date: {
    	type: String, // 2016-04-20
        default: ''
    },
    value: {
    	type: Number,
        default: ''
    }
});

module.exports = mongoose.model('Vegetation', schema);
