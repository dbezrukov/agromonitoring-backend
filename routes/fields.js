var Field = require('../models/field');
var User = require(__coreModules + '/../models/user');

module.exports = function(app, passport) {

	app.get('/fields', function(req, res) {

		User
            .findById(req.user.id)
        	.select({ 
    			'groups': 1,
        	})
        	.populate({
                path: 'groups',
                select: '_id name',
            })
            .exec(function(err, groupsData) {
            
            	if (err) {
                	res.send(500, { error: 'db error' });
                	return;
            	}

            	if (!req.user.addInfo || !req.user.addInfo.activeGroup) {
					res.send(500, { error: 'user must join a company to query fields' });
            	}

            	var activeGroup = req.user.addInfo.activeGroup;
		
		    	Field
		            .find({ group : activeGroup })
		            .sort('name')
		            .fill('lastVegetation')
		            .exec(function(err, fields) {

		            	if (err) {
		                	res.send(500, { error: 'db error' });
		                	return;
		            	}

		            	res.json(fields);
		        });
            })
    });

    app.get('/fields/:id', function(req, res) {

        var fieldId = req.params.id;

        Field
            .findById(fieldId)
            .fill('lastVegetation')
            .exec(function(err, field) {
            
            if (err) {
                res.send(500, { error: 'db error' });
                return;
            }

            res.json(field);
        });

    });

    /* Works as PATCH -> updating passed attributes only */
    app.post('/fields/:id', function(req, res) {

        var fieldId = req.params.id;

    	Field
    		.findByIdAndUpdate(fieldId, { 
    			$set : req.body 
    		}, {
            	new: true
            })
            .exec(function(err, field) {
            
            if (err) {
            	console.log('db error: ' +  err);
                res.send(500, { error: 'db error: ' });
                return;
            }

            res.json(field);
        });
    });

    app.delete('/fields/:id', function(req, res) {

        var fieldId = req.params.id;

        Field
            .findByIdAndRemove(fieldId)
            .exec(function(err) {
            
            if (err) {
                res.send(500, { error: 'db error' });
                return;
            }

            res.json({});
        });

    });
};
