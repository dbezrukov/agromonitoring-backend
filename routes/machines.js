var Machine = require('../models/machine');

module.exports = function(app, passport) {

	app.get('/machines', function(req, res) {

    	Machine
            .find({})
            .sort({ type: 'asc' })
            .exec(function(err, machines) {

            	if (err) {
                	res.send(500, { error: 'db error' });
                	return;
            	}

            	res.json(machines);
        });
    });
};