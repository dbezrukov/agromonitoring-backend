//var _ = require(__coreModules + '/underscore');
//var _str = require(__coreModules + '/underscore.string');

var Company = require('../models/company');

module.exports = function(app, passport) {

	app.get('/companies', function(req, res) {

    	Company
            .find({})
            .sort({ name: 'asc' })
            .exec(function(err, groups) {

            	if (err) {
                	res.send(500, { error: 'db error' });
                	return;
            	}

            	res.json(groups);
        });
    });
};