var http = require('http');
var cheerio = require(__coreModules + '/cheerio');
var iconv = require(__coreModules + '/iconv-lite');

var lastNews = [];
var lastNewsTime = 0

module.exports = function(app, passport) {

	app.get('/news', function(req, res) {

		var currentTime = Date.now();

		// update news every 10 minutes
		if (currentTime - lastNewsTime < 600000) {
			return res.json(lastNews);
		}
		
		getNews(function(err, news) {
    		if (err) {
            	res.send(500, { error: 'news error' });
            	return;
        	}

        	lastNews = news;
        	lastNewsTime = Date.now();

        	res.json(news);
    	})

    	function getNews(callback) {

			var news = [];

			var options = {
				hostname: 'agroobzor.ru',
				path: '/?dn=news&to=all&p=1',
				headers: { 'user-agent': 'Mozilla/5.0' },
				port: 80
    		};

	        var reqNews = http.get(options, function(res) {
				var chunks = [];
			
				res.on('data', function (chunk) {
					chunks.push(chunk);
	        	});
	        
	        	res.on('end', function () {

	        		var buffer = Buffer.concat(chunks);
      				var str = iconv.decode(buffer, 'windows-1251');

					var $ = cheerio.load(str);

					
					var blocks = $('.sitecenter .container');
					var count = 0;
					blocks.each(function(i, td){
						count++;

						// \n acts as children
						var date = td.children[1].children[1].children[1].children[0].data;
						var title = td.children[3].children[1].children[1].children[0].children[0].children[0].data;
						var text = td.children[3].children[1].children[2].children[0].data.replace(/(\r\n|\n|\r)/gm, '');
						var hrefAttr = td.children[5].children[1].children[3].children[0].attribs['href'];
						var href = 'http://agroobzor.ru/' + hrefAttr;

						news.push({ 
							date: date, 
							title: title, 
							text: text,
							href: href
						})
					});

					callback(null, news);
				});
	        });

	        reqNews.on('error', function (err) {
	        	callback(err);
	        });

	        reqNews.end();
    	}

    });
};
