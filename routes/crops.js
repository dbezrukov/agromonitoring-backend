var Crop = require('../models/crop');

module.exports = function(app, passport) {

	app.get('/crops', function(req, res) {

    	Crop
            .find({})
            .sort({ name: 'asc' })
            .exec(function(err, crops) {

            	if (err) {
                	res.send(500, { error: 'db error' });
                	return;
            	}

            	res.json(crops);
        });
    });

    app.get('/crops/:id', function(req, res) {

        var cropId = req.params.id;

        Crop
            .findById(cropId)
            .exec(function(err, crop) {
            
            if (err) {
                res.send(500, { error: 'db error' });
                return;
            }

            res.json(crop);
        });

    });
};