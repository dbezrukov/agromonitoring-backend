var _ = require(__coreModules + '/underscore');
var async = require(__coreModules + '/async');

var Field = require('../models/field');
var Seeding = require('../models/seeding');

module.exports = function(app, passport) {

	app.get('/seedings/stats/:year', function(req, res) {

        var year = req.params.year;

        Seeding
            .find({
            	year: year
            })
            .exec(function(err, seedings) {
            
            if (err) {
                res.send(500, { error: 'db error' });
                return;
            }

			var crops = _.uniq(_.pluck(seedings, 'cropId'));
			var seedingStats = [];

			async.forEach(crops, function(crop, callback){

				var cropSeedings = _.filter(seedings, function(seeding){ return seeding.cropId === crop; });

				function random(low, high) {
    				return Math.floor(Math.random() * (high - low) + low);
				}

				var seedingStat = {
					cropId: crop,
					area: 0, // n/a yet
					yield_productivity_estimated: 100 + random(0, 100),
					yield_estimated: 100 + random(0, 100),
					yield_average: 100 + random(0, 100),
					yield: 100 + random(0, 100),
					fields: []
				}

				cropSeedings.forEach(function(seeding) {
					seedingStat.fields.push(seeding.field);
				})

				Field
					.find({ _id: { $in: seedingStat.fields } })
					.exec(function(err, fields) {

						if (!err && fields) {
							fields.forEach(function(field) {
								seedingStat.area += field.area.cultivated
       						})

       						// rounding
       						seedingStat.area = Math.round(seedingStat.area * 10) / 10;
						}

						seedingStats.push(seedingStat);
    					callback();
                	})

			}, function(err){
 				if(err) { throw err; }

 				res.json(seedingStats);
			});
			
        });
    });

	app.get('/seedings', function(req, res) {

        Seeding
            .find({})
            .exec(function(err, seedings) {
            
            if (err) {
                res.send(500, { error: 'db error' });
                return;
            }

            res.json(seedings);
        });
    });

	app.get('/seedings/:field/:year', function(req, res) {

        var field = req.params.field;
        var year = req.params.year;

        Seeding
            .findOne({
            	field: field,
            	year: year
            })
            .exec(function(err, seeding) {
            
            if (err) {
                res.send(500, { error: 'db error' });
                return;
            }

            if (!seeding) {
            	seeding = {
            		field: field,
            		year: year,
            		cropId: ''
            	}
            }

            res.json(seeding);
        });
    });

    app.post('/seedings/:field/:year', function(req, res) {

    	var field = req.params.field;
        var year = req.params.year;

        Seeding
            .findOneAndUpdate({
            	field: field,
            	year: year
            }, { 
            	$set : req.body 
            }, {
            	upsert: true,
            	new: true
            })
            .exec(function(err, seeding) {
            
            if (err) {
                res.send(500, { error: 'db error' });
                return;
            }

            res.json(seeding);
        });
    });
};
