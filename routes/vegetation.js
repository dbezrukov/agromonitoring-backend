var _ = require(__coreModules + '/underscore');
var async = require(__coreModules + '/async');
var csv = require(__coreModules + '/fast-csv');

var Field = require('../models/field');
var Vegetation = require('../models/vegetation');

var multer  = require(__coreModules + '/multer');
var upload = multer({ inMemory: true });

module.exports = function(app, passport) {

	app.get('/vegetation/:field/:year', function(req, res) {

        var field = req.params.field;
        var year = req.params.year;

        Vegetation
            .find({
            	field: field,
            	date: {
            		$gte:  year      + '-01-01',
            		$lte: (year + 1) + '-01-01'
            	}
            })
            .sort('date')
            .exec(function(err, vegetations) {
            
            if (err) {
                res.send(500, { error: 'db error' });
                return;
            }

            var annualVegetation = {
            	field: field,
            	year: year,
            	data: highchartsData(vegetations)
            }

            res.json(annualVegetation);

            function highchartsData(array) {

            	// produces an array like [[1460764800000,0.215],[1460937600000,0.232],[1461024000000,0.238]] 
            	if (!array) {
            		return [];
            	}

            	return array.map(function(obj) {
	  				return [
	  					(new Date(obj.date)).getTime(),
	  					obj.value
	  				]
				})
            }

        });
    });
};
