var Equipment = require('../models/equipment');

module.exports = function(app, passport) {

	app.get('/equipment', function(req, res) {

    	Equipment
            .find({})
            .sort({ type: 'asc' })
            .exec(function(err, equipment) {

            	if (err) {
                	res.send(500, { error: 'db error' });
                	return;
            	}

            	res.json(equipment);
        });
    });
};